#include "carnica.h"

// CARNIÇA DO TIPO INTEIRO

CarnicaInt::CarnicaInt()
{
    valorCarnica = 0;
}

CarnicaInt::CarnicaInt(int valor)
{
    valorCarnica = valor;
}

bool CarnicaInt::defineCarnica(int valor)
{
    valorCarnica = valor;
    return true;
}

bool CarnicaInt::defineCarnica(Carnica& carnicaValor)
{
    Carnica* carnicaValorInt = carnicaValor.pegaCarnica(TipoCarnica::CARNICA_INT);

    if (carnicaValorInt == nullptr)
        return false;
    else
        return this->defineCarnica(static_cast<CarnicaInt*>(carnicaValorInt)->pegaValor());
}

Carnica *CarnicaInt::pegaCarnica(TipoCarnica tipo)
{
    switch(tipo)
    {
    case (TipoCarnica::CARNICA_INT):
        return this;
    case (TipoCarnica::CARNICA_CHR):
    {
        CarnicaChr* convertidaChr = new CarnicaChr( static_cast<char>( pegaValor() ) );
        return convertidaChr;
    }
    default:
        return nullptr;
    }
}

int CarnicaInt::pegaValor()
{
    return valorCarnica;
}

// CARNIÇA DO TIPO CARACTERE

CarnicaChr::CarnicaChr()
{
    valorCarnica = 0;
}

CarnicaChr::CarnicaChr(char valor)
{
    valorCarnica = valor;
}

bool CarnicaChr::defineCarnica(char valor)
{
    valorCarnica = valor;
    return true;
}

bool CarnicaChr::defineCarnica(Carnica &carnicaValor)
{
    Carnica* carnicaValorChr = carnicaValor.pegaCarnica(TipoCarnica::CARNICA_CHR);

    if (carnicaValorChr == nullptr)
        return false;
    else
        return this->defineCarnica(static_cast<CarnicaChr*>(carnicaValorChr)->pegaValor());
}

Carnica *CarnicaChr::pegaCarnica(TipoCarnica tipo)
{
    switch(tipo)
    {
    case (TipoCarnica::CARNICA_INT):
    {
        CarnicaInt* convertidaInt = new CarnicaInt( static_cast<int>( pegaValor() ) );
        return convertidaInt;
    }
    case (TipoCarnica::CARNICA_CHR):
        return this;
    default:
        return nullptr;
    }
}

char CarnicaChr::pegaValor()
{
    return valorCarnica;
}


bool Carnica::defineCarnica() { return false; }
