﻿#ifndef CARNICA_H
#define CARNICA_H

//! Enumeração os tipos de carniça
enum TipoCarnica
{
    CARNICA_INT, ///< Carniça do tipo inteiro
    CARNICA_CHR, ///< Carniça do tipo caractere/byte
    CARNICA_LNG, ///< Carniça do tipo long
    CARNICA_FLT, ///< Carniça do tipo flutuante (double)
    CARNICA_STR  ///< Carniça do tipo string
};

//! Classe base de carniça
class Carnica
{
public:
    virtual ~Carnica() = 0;

    bool defineCarnica();
    virtual Carnica* pegaCarnica(TipoCarnica tipo) = 0;

};

class CarnicaInt : public Carnica
{
public:
    /*!
     * \brief Construtor de carniça de tipo inteiro padrão.
     * Armazena 0 no valor.
     */
    CarnicaInt();

    /*!
     * \brief Construtor de carniça de tipo inteiro com valor.
     * \param valor Valor a ser armazenado na carniça.
     */
    CarnicaInt(int valor);

    /*!
     * \brief Substitui o valor atualmente armazenado na carniça
     * \param valor Novo valor a ser armazenado na carniça.
     * \return Se a atribuição teve sucesso
     */
    bool defineCarnica(int valor);

    /*!
     * \brief Substitui o valor atualmente armazenado na carniça pelo de outra carniça
     * \param carnicaValor Referência a carniça com o novo valor a ser armazenado
     * \return Se a atribuição teve sucesso
     */
    bool defineCarnica(Carnica& carnicaValor);

    /*!
     * \brief Retorna carniça convertida
     * \param tipo Tipo de carniça desejado
     * \return Ponteiro para carniça do tipo desejado
     */
    Carnica* pegaCarnica(TipoCarnica tipo);

    /*!
     * \brief Retorna o valor armazenado na carniça
     * \return Valor armazenado na carniça
     */
    int pegaValor();

private:
    int valorCarnica; ///< Valor armazenado pela carniça
};

class CarnicaChr : public Carnica
{
public:
    /*!
     * \brief Construtor de carniça do tipo caractere padrão.
     */
    CarnicaChr();

    /*!
     * \brief Construtor de carniça do tipo caractere com valor.
     * \param valor Valor a ser armazenado na carniça.
     */
    CarnicaChr(char valor);

    /*!
     * \brief Substitui o valor atualmente armazenado na carniça.
     * \param valor Novo valor a ser armazenado na carniça.
     * \return Se a atribuição teve sucesso
     */
    bool defineCarnica(char valor);

    /*!
     * \brief Substitui o valor atualmente armazenado na carniça pelo de outra carniça
     * \param carnicaValor Referência a carniça com o novo valor a ser armazenado
     * \return Se a atribuição teve sucesso
     */
    bool defineCarnica(Carnica& carnicaValor);

    /*!
     * \brief Retorna carniça convertida
     * \param tipo Tipo de carniça desejado
     * \return Ponteiro para carniça do tipo desejado
     */
    Carnica* pegaCarnica(TipoCarnica tipo);

    /*!
     * \brief Retorna o valor armazenado na carniça
     * \return Valor armazenado na carniça
     */
    char pegaValor();

private:
    char valorCarnica; ///< Valor armazenado pela carniça
};

#endif // CARNICA_H
